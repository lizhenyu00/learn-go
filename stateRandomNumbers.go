package main

import (
	"math/rand"
)

const MaxRand = 16

/*
StateRandomNumbers生成一些不大于MaxRand的非负随机整数，并统计返回小于
和大于MaxRand/2的随机数个数，输入参数numRands指定了生成的随机数的总数
*/
func StateRandomNumbers(numRands int) (int, int) {
	var a, b int
	for i := 0; i < numRands; i++ {
		// 一个if-else条件控制代码块
		if rand.Intn(MaxRand) < MaxRand/2 {
			a = a + 1
		} else {
			b++
		}
	}
	return a, b
}
